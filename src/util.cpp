#include "util.h"

#include "iostream"
#include "vector"
#include "string"
#include "fstream"
#include "sstream"

using namespace std;

string book_to_csv(book b)
{
  return b.isbn + "," +
         b.title + "," +
         b.author.first_name + "," +
         b.author.middle_name + "," +
         b.author.last_name + "," +
         to_string(b.date_bought.tm_mon) + "," +
         to_string(b.date_bought.tm_mday) + "," +
         to_string(b.date_bought.tm_year);
}

void save_as_csv_file(string file, vector<book> books)
{
  ofstream bookshelf_file(file);

  for (book b : books)
  {
    bookshelf_file << book_to_csv(b) << endl;
  }

  cout << "\n[i] SAVED UPDATE IN " << file << ".\n";
}

vector<book> read_from_csv_file(string file)
{
  vector<book> books{};
  cout << "\n[i] READING BOOKS FROM " << file << "...\n";

  ifstream bookshelf_file(file);

  if (!bookshelf_file.is_open())
  {
    throw std::runtime_error("Could not open file");
  }

  string book_line = "";

  if (bookshelf_file.good())
  {
    while (getline(bookshelf_file, book_line))
    {
      stringstream ss(book_line);
      book b;
      getline(ss, b.isbn, ',');
      getline(ss, b.title, ',');

      getline(ss, b.author.first_name, ',');
      getline(ss, b.author.middle_name, ',');
      getline(ss, b.author.last_name, ',');

      string month, day, year;
      getline(ss, month, ',');
      getline(ss, day, ',');
      getline(ss, year, ',');
      b.date_bought.tm_mon = stoi(month);
      b.date_bought.tm_mday = stoi(day);
      b.date_bought.tm_year = stoi(year);

      books.push_back(b);
    }
  }

  return books;
}

string load_banner()
{
  ifstream banner_file("./banner.txt");

  if (!banner_file.is_open())
  {
    throw std::runtime_error("Could not open file");
  }

  string banner = "";

  if (banner_file.good())
  {
    string line = "";
    while (getline(banner_file, line))
    {
      banner += line + "\n";
    }
  }

  return banner;
}