#include "vector"
#include "string"

#include "core-data.h"

using namespace std;

void save_as_csv_file(string file, vector<book> books);

vector<book> read_from_csv_file(string file);

string load_banner(void);