#include <iostream>
#include <map>
#include "vector"
#include "ctime"
#include "conio.h"
#include "string"
#include "sstream"
#include "limits"

#include "util.h"

using namespace std;

const int INT_MAX = numeric_limits<int>::max();

vector<book> books = {};
string searchText;
map<int, book> bookFound;

void view_book(void);
void view_book_search(void);
void add_new_book(void);
void update_book(void);
void search_book(void);
void delete_book(void);

int main(int argc, char *argv[])
{
  books = read_from_csv_file("bookshelf-db.csv");

  char operation = ' ';
  while (operation != '6')
  {
    system("cls");
    cout << load_banner();

    if (operation == '3')
    {
      view_book_search();
    }
    else
    {
      view_book();
    }

    cout << "|                                                                        |" << endl
         << "|      ( 1 ) Add new book    ( 3 ) Search book    ( 5 ) Delete book      |" << endl
         << "|      ( 2 ) Update book     ( 4 ) Reset search   ( 6 ) Exit             |" << endl
         << "|                                                                        |" << endl
         << "+------------------------------------------------------------------------+" << endl
         << "[?] Enter operation: ";

    cin >> operation;

    bool modOption = operation == '2' || operation == '3' || operation == '4' || operation == '5';
    if (books.size() == 0 && modOption)
    {
      cout << "Invalid operation for an empty book collection." << endl;
      system("PAUSE");
      operation = ' ';
      continue;
    }

    switch (operation)
    {
    case '1':
      add_new_book();
      break;
    case '2':
      update_book();
      break;
    case '3':
      search_book();
      break;
    case '4':
      operation = ' ';
      break;
    case '5':
      delete_book();
      break;
    }
  }
}

void view_book()
{
  cout << "+------------------------------------------------------------------------+" << endl
       << "|                                                                        |" << endl
       << "|                                   BOOKS                                |" << endl
       << "|                                   -----                                |" << endl
       << "|                                                                        |" << endl
       << "  Total: " << books.size() << endl
       << "|                                                                        |" << endl
       << "|------------------------------------------------------------------------|" << endl;

  int i = 1;
  for (book b : books)
  {
    cout << "|                                                                        |" << endl
         << "+--                                                                    --+" << endl
         << "|                                                                        |" << endl
         << "  (" << i++ << ") ISBN: " << b.isbn << endl
         << "      Title: " << b.title << endl
         << "      Author: " << b.author.first_name << " " << b.author.last_name << endl
         << "      Bought on: " << b.date_bought.tm_mon << "/" << b.date_bought.tm_mday << "/" << b.date_bought.tm_year << endl
         << endl;
  }

  cout << "|                                                                        |" << endl
       << "+------------------------------------------------------------------------+" << endl;
}

void view_book_search()
{
  cout << "+------------------------------------------------------------------------+" << endl
       << "|                                                                        |" << endl
       << "|                            SEARCH RESULTS                              |" << endl
       << "|                            --------------                              |" << endl
       << "  Search text: " << searchText << endl
       << "  Books found: " << bookFound.size() << endl
       << "|                                                                        |" << endl
       << "|                                                                        |" << endl;
  for (const auto &b : bookFound)
  {
    cout << "  (" << b.first + 1 << ") ISBN: " << b.second.isbn << endl
         << "      Title: " << b.second.title << endl
         << "      Author: " << b.second.author.first_name << " " << b.second.author.last_name << endl
         << "      Bought on: " << b.second.date_bought.tm_mon << "/" << b.second.date_bought.tm_mday << "/" << b.second.date_bought.tm_year << endl
         << endl;
  }

  cout << "|                                                                        |" << endl
       << "+------------------------------------------------------------------------+" << endl;
}

void add_new_book()
{
  book nb;

  cout << "[i] Fill out the new book's info." << endl;
  cout << "*";
  cout << " "
       << "ISBN: ";
  cin >> nb.isbn;

  cin.clear();
  cin.ignore(INT_MAX, '\n');
  cout << "  "
       << "Title: ";
  getline(cin, nb.title);

  cout << "  "
       << "Author (FN MN LN): ";
  cin >> nb.author.first_name >> nb.author.middle_name >> nb.author.last_name;

  cin.clear();
  cin.ignore(INT_MAX, '\n');
  cout << "  "
       << "Date bought (M D Y): ";
  cin >> nb.date_bought.tm_mon >> nb.date_bought.tm_mday >> nb.date_bought.tm_year;

  books.push_back(nb);
  save_as_csv_file("bookshelf-db.csv", books);
  printf("\n[i] Book Added (%s by %s %s)\n", nb.title.c_str(), nb.author.first_name.c_str(), nb.author.last_name.c_str());

  cin.clear();
  cin.ignore(INT_MAX, '\n');

  cout << endl;
}

void update_book()
{
  cout << "[?] Enter number of book to update: ";

  string id = "";
  cin >> id;

  book ubook = books[stoi(id) - 1];

  cout << "[i] NOTE: Type a dot (.) to leave the field unedited." << endl
       << endl;

  printf("ISBN (%s): ", ubook.isbn.c_str());
  string isbn = "";
  cin >> isbn;
  if (isbn != ".")
  {
    ubook.isbn = isbn;
  }

  printf("Title (%s): ", ubook.title.c_str());
  string title = "";
  cin.clear();
  cin.ignore(INT_MAX, '\n');
  getline(cin, title);
  if (title != ".")
  {
    ubook.title = title;
  }

  printf("Author (%s,%s): ", ubook.author.last_name.c_str(), ubook.author.first_name.c_str());
  string name = "";
  cin >> name;
  if (name != ".")
  {
    stringstream ss(name);
    vector<string> fullname{};
    while (ss.good())
    {
      string substr;
      getline(ss, substr, ',');
      fullname.push_back(substr);
    }
    ubook.author.first_name = fullname.at(1);
    ubook.author.last_name = fullname.at(0);
  }

  printf("Bought date (%d/%d/%d): ",
         ubook.date_bought.tm_mon,
         ubook.date_bought.tm_mday,
         ubook.date_bought.tm_year);
  string date = "";
  cin >> date;
  if (date != ".")
  {
    stringstream ss(date);
    vector<string> date{};
    while (ss.good())
    {
      string substr;
      getline(ss, substr, '/');
      date.push_back(substr);
    }
    ubook.date_bought.tm_mon = stoi(date.at(0));
    ubook.date_bought.tm_mday = stoi(date.at(1));
    ubook.date_bought.tm_year = stoi(date.at(2));
  }

  books[stoi(id) - 1] = ubook;

  save_as_csv_file("bookshelf-db.csv", books);
  printf("\n[i] Book Updated (%s by %s %s)\n", ubook.title.c_str(), ubook.author.first_name.c_str(), ubook.author.last_name.c_str());
}

void search_book()
{
  bookFound.clear();

  cin.clear();
  cin.ignore(INT_MAX, '\n');

  cout << "[?] Search: ";
  getline(cin, searchText);

  int i = 0;
  for (book b : books)
  {
    string fullName = b.author.first_name + " " + b.author.middle_name + " " + b.author.last_name;
    bool matchedBookInfo = (b.title.find(searchText) != string::npos) ||
                           (fullName.find(searchText) != string::npos) ||
                           (b.isbn.find(searchText) != string::npos);

    if (matchedBookInfo)
    {
      bookFound[i] = b;
    }

    i++;
  }
}

void delete_book()
{

  cout << "[?] Enter book's number (id) to delete: ";
  string id;
  cin >> id;

  book b = books[stoi(id) - 1];

  cout << "[?] Are sure you want to delete '" << b.title << "' by " << b.author.first_name << " " << b.author.last_name << "? (y/n): ";
  string option;
  cin >> option;

  if (option == "y")
  {
    books.erase(books.begin() + (stoi(id) - 1));
    cout << "[i] Book deleted.";
    save_as_csv_file("bookshelf-db.csv", books);
  }
}