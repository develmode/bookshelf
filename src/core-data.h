#include "string"
#include "ctime"

using namespace std;

struct author
{
  string first_name;
  string last_name;
  string middle_name;
};

struct book
{
  string isbn;
  string title;
  author author;
  tm date_bought;
};
