# bookshelf

A console-based bookshelf app.

This is a school project for learning the basics of C/C++. I used the standard container library from C++ for a container-like structure instead of a plain array, while using a CSV (Comma Separated Value) file for a database. Since, the object-oriented concepts are not yet covered in the class, I used structs for basic modularization.

This project is built using MinGW with VS Code as the code editor. It's not tested using CodeBlocks or other C++ IDE, so for lesser hassle I would recommend using VS Code with MinGW installed in your PC.

You can start playing around **bookshelf** by downloading the zip file from the **release** section. It contains the executable binary file (`bookshelf.exe`), the ascii-art text for banner, and the `bookshelf-db.csv` file for data-storage. Since **bookshelf** is a console app you can only run it in a console by `cd`-ing your way to the directory where you extracted the zip file and from there typed in the `bookshelf` or `bookshelf.exe` command. When successful you should see this banner printed in your console:

![](bookshelf-banner.png)

## Download

- [bookshelf-v0.1.1-alpha-win64.zip](https://github.com/rvalledorjr/bookshelf/releases/download/v0.1.1-alpha/bookshelf-v0.1.1-alpha-win64.zip) - Contains **bookshelf**'s executable file. Windows is the currently supported platform for now.
- [bookshelf-in-action.mp4](https://github.com/rvalledorjr/bookshelf/releases/download/v0.1.1-alpha/bookshelf-in-action.mp4) - A screen record video showcasing bookshelf's functionalities.

## Project Structure

- `.vscode` - It contains the needed configs so that VS Code can compile and build the project. You can build the project by using this command: `CTRL` + `SHIFT` + `B`.
- `bin` - It's where the build artifacts go, like the `bookshelf.exe` file.
- `include` - This is where 3rd party libraries went if it is in a source code form.
- `lib` - This is where 3rd party libraries went if it is in a binary form.
- `src` - The is where the actual source code of the project reside.
  - `core-data.h` - It contains the definition of the core data/modules used in this project.
  - `util.h` and `util.cpp` - The files contains the definition and implementation of the utility functions used in the project. Currently, it's where the CSV loader, exporter, and simple parser are defined and implemented.
  - `main.cpp` - The main entry point of the program.

## App Flowchart

The following diagram shows the general flow of **bookshelf**. Each major functionalities of the app are abstracted in their respective subroutines. I chose to document only those subroutines that have a more involved processing underneath and leave the rest that are simpler.

Note. The flowcharts shown here captures ONLY the essential "logic" of the app's code. There are details in the app's code that are no longer represented here so as to keep the flowchart clearer in its intent.

<img src="flowchart.png" alt="flowchart.jpg"  />

The `add_book` and `update_book` subroutines both follow a sequential flow with differences being that the `add_book` subroutine pushes a book type data to a vector of books after filling up the information related to books; while the `update_book` subroutine pull a data from a vector of books using an index and update that data's fields.

Following is the implementation for the `search-book` subroutine:

![](search-book-fc.png)
